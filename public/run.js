

document.addEventListener("DOMContentLoaded", function(){

    // Get objects from DOM
    var inputText = document.getElementById("inputText");
    var outputText = document.getElementById("outputText");
    var copyButton = document.getElementById("buttonCopyClipboard");
    
    // Local definitions of functions

    function getTable(str) {
        var returnValue = '';
        var separateLines = str.split(/\r?\n|\r|\n/g);
        var firstLine = separateLines[0];
        var columns = firstLine.split(/\t/g);
        
        // Open value statement
        returnValue = 'DATA(lv_string) = VALUE #( ';
        
        // Add lines
        for (let i = 1; i < separateLines.length; i++) {
            
            var splitLine = separateLines[i].split(/\t/g);

            // Only take lines into account that have the sam 
            // layout as the header line
            if (splitLine.length == columns.length) {

                // Create key-value pairs: "key = 'value'"
                var combinedKeyValue = columns.map((e, i) => e + " = '" + splitLine[i] + "'" );

                // Begin table line
                var currentLine = '\n( ';

                for (let j = 0; j < combinedKeyValue.length; j++){
                    // Limit print lines to 150 chars in order to match
                    // editor length restriction
                    if (currentLine.length + combinedKeyValue[j].length < 149) {
                        currentLine += ' ' + combinedKeyValue[j];
                    } else {
                        // Complete print line
                        returnValue += currentLine + '\n';
                        currentLine = combinedKeyValue[j];
                    }
                }

                // Add remaining value to output
                if (currentLine != '') {
                    returnValue += currentLine;
                }
                
                // End table line
                returnValue += ' )';
            };
        }

        // Close value statement
        returnValue += '\n).';
        return returnValue;
    }


    inputText.addEventListener("input", function(){
        var newText = getTable(inputText.value);
        outputText.value = newText;
    });

    copyButton.addEventListener("click", function(){
        outputText.select();
        outputText.setSelectionRange(0, 99999); /*For mobile devices*/
        document.execCommand("copy");
    });
});

